# Reinforcement Learning
 
Python version: 3. Required libraries: matplotlib and numpy    
Run: 'python reinforcement.py'  

**There will be no output, however the following flags will add output:**
Show the learning curve: --learning_curve True  
Show the preferred directions stored in the weights: --directions True  

**Optimisation methods:**  
Punish the robot when it walks out of the rooom: --punish_boundary True  
Q_learning algorithm: --q_learning True  

**Enviroment options:**  
Room side length (NxN size room): --room_length N  
Number of actions (4 or 8): --num_actions N  


**Compare learning curves:**  
First run the reinforcement learning algorithm with this flag: 'python reinforcement.py --save_filename [filename.npy]'  
Second compare the learning curves of N runs: 'python plot_learning_curves.py [filename1.npy] [...] [filenameN.npy]'  




Code adapted from the code provided in labs
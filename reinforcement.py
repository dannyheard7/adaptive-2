import numpy as np
import matplotlib.pyplot as plt
import argparse

def print_directions(weights, N_actions, room_length, reward_location):
    weights = weights.transpose()
    pref_direction = np.argmax(weights, axis=1)

    pref_direction = pref_direction.reshape(room_length, room_length, order='F')

    data = np.empty(pref_direction.shape, dtype=np.unicode)

    if N_actions == 8:
        direction = {0: "↑", 1: "→", 2: "↓", 3: "←", 4: "↗", 5: "↘", 6: "↙", 7: "↖"}
    else: 
        direction = {0: "↑", 1: "→", 2: "↓", 3: "←"}

    for i in range(pref_direction.shape[0]):
        data[i] = ["R" if reward_location == (i,j) else direction[x] for j, x in enumerate(pref_direction[i], 0)]

    print(data)


def homing_nn(room_length, end_location, n_trials, learning_rate, eps, gamma, N_actions, punish_boundary, q_learning):
    # Solving homing task with on-policy TD (SARSA)
    N_states = room_length * room_length
    states_matrix = np.eye(N_states)
    n_steps = N_states * 2

    if N_actions == 8:
        # 1->N 2->E 3->S 4->W 5->NE 6->SE 7->SW 8->NW
        action_row_change = np.array([-1,0,1,0,-1,1,1,-1])               #number of cell shifted in vertical as a function of the action
        action_col_change = np.array([0,1,0,-1,1,1,-1,-1])              #number of cell shifted in horizontal as a function of the action
    else:
        # 1->N 2->E 3->S 4->W
        action_row_change = np.array([-1,0,1,0]) 
        action_col_change = np.array([0,1,0,-1])               
        N_actions = 4

    s_end = np.ravel_multi_index(end_location, dims=(room_length,room_length),order='F')  #terminal state. Conversion in single index

    ## Rewards
    end_reward = 10    #only when the robot reaches the charger, sited in End state
    boundary_reward = -1 if punish_boundary else 0    # If robot goes out of the room, apply a punishment                      

    ## Variables
    weights = np.zeros((N_actions,N_states))
    learning_curve = np.zeros((1,n_trials))
    Rewards = np.zeros((1,n_trials))

    # Start trials
    for trial in range(n_trials):
        # Initialization
        Start = np.array([np.random.randint(room_length), np.random.randint(room_length)])   #random start
        s_start = np.ravel_multi_index(Start,dims=(room_length, room_length),order='F')      #conversion in single index
        state = Start                                                   #set current state
        s_index = s_start                                               #conversion in single index
        step = 0
        
        r_old = 0
        Q_old = 0
        input_old = np.zeros((N_states,1))
        output_old = np.zeros((N_actions,1))

        # Start steps
        while s_index != s_end and step <= n_steps:
            step += 1
            learning_curve[0,trial] = step

            r = 0
            input_vector = states_matrix[:,s_index].reshape(N_states,1)         #convert the state into an input vector

            #compute Qvalues. Qvalue=logsig(weights*input). Qvalue is 2x1, one value for each output neuron
            Q = 1 / ( 1 + np.exp( - weights.dot(input_vector)))    #Qvalue is 2x1 implementation of logsig

            #eps-greedy policy implementation
            greedy = (np.random.rand() > eps)               #1--->greedy action 0--->non-greedy action
            if q_learning or greedy:
                action = np.argmax(Q)                           #pick best action
            else:
                action = np.random.randint(N_actions)           #pick random action

            state_new = np.array([0,0])
            
            #move into a new state
            state_new[0] = state[0] + action_row_change[action]
            state_new[1] = state[1] + action_col_change[action]

            #Bounded position. Consider also the option to give a negative reward
            if state_new[0] < 0:
                state_new[0] = 0
                r += boundary_reward
            if state_new[0] >= room_length:
                state_new[0] = room_length-1
                r += boundary_reward
            if state_new[1] < 0:
                state_new[1] = 0
                r += boundary_reward
            if state_new[1] >= room_length:
                state_new[1] = room_length-1
                r += boundary_reward

            s_index_new = np.ravel_multi_index(state_new,dims=(room_length,room_length),order='F')  #conversion in a single index

            #store variables for sarsa computation in the next step
            output = np.zeros((N_actions,1))
            output[action] = 1

            state[0] = state_new[0]
            state[1] = state_new[1]
            s_index = s_index_new

            ## TODO: check if state is terminal and update the weights consequently
            if s_index == s_end:
                r += end_reward
            
            if step != 1:
                dw = learning_rate * (r_old - Q_old + gamma * Q[action]) * output_old.dot(input_old.T)
                weights += dw

            #update variables
            input_old = input_vector
            output_old = output
            Q_old = Q[action]
            r_old = r

        # Update weights for the terminal state
        dw = learning_rate * (r_old - Q_old) * output_old.dot(input_old.T)
        weights += dw

    return weights, learning_curve


def reinforcement_learning(room_length=5,  N_actions=4, punish_boundary=False, learning_curve=False, directions=False, q_learning=False, save_filename=None):
    #Parameter setup
    nTrials = 100        # should be integer >0
    learningRate = 0.8  # should be real, Greater than 0
    epsilon = 0.2 # should be real, Greater or Equal to 0; epsilon=0 Greedy, otherwise epsilon-Greedy
    gamma = 0.3        # should be real, positive, smaller than 1

    repetitions = 100   # number of episodes, should be integer, greater than 0; for statistical reasons

    reward_location = (np.random.randint(0, room_length), np.random.randint(0, room_length))

    learningCurves = np.zeros((repetitions,nTrials))  # reward matrix. each row contains rewards obtained in one episode
    weights = np.zeros((repetitions, N_actions, room_length*room_length))

    # Start iterations over episodes
    for j in range(repetitions):
        weights[j,:], learningCurves[j,:] = homing_nn(room_length, reward_location, nTrials, learningRate, epsilon, gamma, N_actions, punish_boundary, q_learning)

    weights = np.mean(weights, axis=0)

    if directions:
        print_directions(weights, N_actions, room_length, reward_location)

    if save_filename != None:
        np.save(save_filename, learningCurves)

    # Plot the average reward as a function of the number of trials --> the average has to be performed over the episodes
    if learning_curve:
        fig, axarr = plt.subplots()
        means = np.mean(learningCurves, axis = 0)
        errors = 2 * np.std(learningCurves, axis = 0) / np.sqrt(repetitions) # errorbars are equal to twice standard error i.e. std/sqrt(samples)
        plt.errorbar(np.arange(nTrials), means, errors, 0, elinewidth = 0.8, capsize = 1, alpha =0.8)
        plt.title("SARSA Homing Task Learning Curve - 8 Actions")
        plt.xlabel('Trial')
        plt.ylabel('Number of steps')
        plt.tick_params(axis = 'both', which='major', labelsize = 14)
        fig.tight_layout()
        plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--directions', type=bool)
    parser.add_argument('--room_length', type=int, default=5)
    parser.add_argument('--num_actions', type=int, default=4) 
    parser.add_argument('--learning_curve', type=bool, default=False)
    parser.add_argument('--punish_boundary', type=bool, default=False)
    parser.add_argument('--q_learning', type=bool, default=False)
    parser.add_argument('--save_filename', default=None)

    args = parser.parse_args()                                      

    reinforcement_learning(args.room_length, args.num_actions, args.punish_boundary, args.learning_curve, args.directions, args.q_learning, args.save_filename)
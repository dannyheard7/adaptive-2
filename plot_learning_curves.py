import numpy as np
import matplotlib.pyplot as plt
import sys
import os


if __name__ == '__main__':
    nTrials = 100 
    repetitions = 100

    fig, axarr = plt.subplots()

    for file in sys.argv[1:]:
        learningCurves = np.load(file)   
        name, _ = os.path.splitext(file) 
        name = name.replace("_", " ")

        means = np.mean(learningCurves, axis = 0)
        errors = 2 * np.std(learningCurves, axis = 0) / np.sqrt(repetitions) # errorbars are equal to twice standard error i.e. std/sqrt(samples)
        plt.errorbar(np.arange(nTrials), means, errors, 0, elinewidth = 0.8, capsize = 1, alpha =0.8, label=name)

    plt.legend(loc='upper right')
    plt.title("Comparison of learning curves")
    plt.xlabel('Trial')
    plt.ylabel('Number of steps')
    plt.tick_params(axis = 'both', which='major', labelsize = 14)
    fig.tight_layout()
    plt.show()